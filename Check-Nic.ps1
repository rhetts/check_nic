<#
.SYNOPSIS
Register's DNS
.DESCRIPTION
Checks to see if there an a failure to register DNS error in the Event Logs (ID 8118).
If so, it will manually run the powershell version of "ipconfig /registerdns"
.NOTES
Name: Check-Nic.ps1
Author: Rhett Sullivan
Version: 1.0.0
Release Date: 2 Mar 2020
.EXAMPLE
.\Check-Nic
Description:
Checks to see if there an a failure to register DNS error in the Event Logs (ID 8118).
If so, it will manually run the powershell version of "ipconfig /registerdns"
#>



####################### Variables #######################

$LogPath = "D:\Scripts\Logs\"
$LogName = "NicRegistration.log"
$LogFile = $LogPath + $LogName
$RecordsPath = "D:\Scripts\Files\"
$RecordsName = "$(Get-Date -f yyyyMMdd-HHmmss)_DnsCacheRecords.csv"
$Records = $RecordsPath + $RecordsName
$BlankLine = ' '
$TodaysDate = (Get-Date).ToLongDateString()

$NicRegistrationFailed = $NULL
$NicInstanceId = $NULL


####################### Functions #######################

function Write-Log {
    Param(
        $Message
    )
    function TS {Get-Date -Format 'HH:mm:ss'}
    "[$(TS)]     $Message" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
}


Function Get-LogStatus {
    # Checks to see if a log file exists and if not creates one
    If (Test-Path $LogFile) {
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
 #       $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
    }
    Else {
        New-Item $LogFile -Force -ItemType File
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
 #       $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
    }
}


Function RegistrationCheck {
    $yesterday = (Get-Date) - (New-TimeSpan -Day 1)
    $NicRegistrationFailed = Get-WinEvent -FilterHashTable @{LogName='System'; ID=8018; StartTime=$yesterday}
    [string]$NicInstanceId = $NicRegistrationFailed.Id
    If ($NicInstanceId -eq 8018) {
        Write-Log "Event Log shows error registering dns within past day."
        Set-DnsClient -InterfaceIndex 3 -RegisterThisConnectionsAddress $true;
        Write-Log "Registering Client."}
    Else {Write-Log "Registration already up to date."}
}

#######################  Script Commands #######################

Get-LogStatus
Write-Log "----- Checking DNS Registration -----"
Get-DnsClientCache | Export-CSV -Path $Records -NoTypeInformation
Clear-DnsClientCache
RegistrationCheck
Write-Log "----- DNS Registration Check Completed -----"

Exit