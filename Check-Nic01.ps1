



















####################### Variables #######################

$LogPath = "C:\Scripts\Logs\"
$LogName = "NicRegistration.log"
$LogFile = $LogPath + $LogName
$BlankLine = ' '

$NicRegistrationFailed = $NULL
$NicRegistrationFailed = Get-EventLog system -after (get-date).AddDays(-1) | where {$_.InstanceId -eq 8008}

####################### Functions #######################

function Write-Log {
    Param(
        $Message
    )
    function TS {Get-Date -Format 'HH:mm:ss'}
    "[$(TS)]     $Message" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
}


Function Get-LogStatus {
    # Checks to see if a log file exists and if not creates one
    If (Test-Path $LogFile) {
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        Write-Log 'Running PaperCut - Add Initial MPA Quotas Script'
    }
    Else {
        New-Item $LogFile -Force -ItemType File
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        Write-Log 'Running PaperCut - Add Initial MPA Quotas Script'
    }
}


Function RegistrationCheck {
    [string]$NicInstanceId = $NicRegistrationFailed.InstanceId
    If ($NicInstanceId -eq 8008) {
        Write-Log "Registering Client";
        Register-DnsClient}
    Else {Write-Log "Registration already up to date"; Exit}
}

#######################  Script Commands #######################

Get-LogStatus
Write-Log "----- Checking DNS Registration -----"
Get-Command -Module DNSClient
Clear-DnsClientCache
RegistrationCheck
Exit

